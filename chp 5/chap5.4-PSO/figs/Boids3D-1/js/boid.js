//	xxx  为内部变量.
//	xxx_ 为成员变量.
// _xxx  为函数参数.

var Boid = function() {
			
	var acceleration,	//所有的规则加起来后得到的向量.
		
		width = 500,	//世界的大小(宽度为500, 高度为500, 深度为200).
		heigth = 500,
		depth = 500,
		
		goal,	//TODO ??
		
		neighborhoodRadius = 100,	//视野范围.
		
		maxSpeed = 4,	//最大速度.
		maxSteerForce = 0.1;	//规则最大加速值.
		
		this.position_ = new THREE.Vector3();	//Boid 当前的位置.
		this.velocity_ = new THREE.Vector3();	//Boid 当前的速度.
		
		acceleration = new THREE.Vector3();		//初始化规则向量.
		
		this.setWorldSize = function(_width, _height, _depth) {
			width = _width;
			height = _height;
			depth = _depth;
		}
		
		//用于计算 acceleration.
		this.flock = function(_boids) {
		
			//用来存储所有在视野范围内的Boid.
			var curBoids = [];
			
			for(var i = 0, len = _boids.length; i < len; i++) {
			
				if (Math.random() > 0.6) continue;
				var boid = _boids[i];
				var distance = boid.position_.distanceTo(this.position_);
				if(distance > 0 && distance <= neighborhoodRadius) {
					curBoids.push(boid);
				}
			}
			//console.log(curBoids.length);
			if(curBoids.length > 0) {
				acceleration.add(this.alignment(curBoids));
				acceleration.add(this.cohesion(curBoids));
				acceleration.add(this.separation(curBoids));
			}
		}
		
		// alignment 规则.
		this.alignment = function(_boids) {
			var boid,
				result = new THREE.Vector3();
			
			for(var i = 0, len = _boids.length; i < len; i++) {
				boid = _boids[i];
				result.add(boid.velocity_);
			}
			
			result.divideScalar(len);
			
			var l = result.length();
			if ( l > maxSteerForce ) {
				result.divideScalar( l / maxSteerForce );
			}
			
			return result;
		}
		
		// cohesion 规则.
		this.cohesion = function(_boids) {
			var boid,
				result = new THREE.Vector3();
			
			for(var i = 0, len = _boids.length; i < len; i++) {
				boid = _boids[i];
				result.add(boid.position_);
			}
			
			result.divideScalar(len);
			
			result.sub(this.position_);
			
			var l = result.length();
			if ( l > maxSteerForce ) {
				result.divideScalar( l / maxSteerForce );
			}
			
			return result;
		}
		
		// separation 规则.
		this.separation = function(_boids) {
			var boid,
				result = new THREE.Vector3();
				repulse = new THREE.Vector3();
				
			for(var i = 0, len = _boids.length; i < len; i++) {
				boid = _boids[i];
				var distance = boid.position_.distanceTo(this.position_);
				repulse.subVectors(this.position_, boid.position_);
				repulse.normalize();
				repulse.divideScalar(distance);
				result.add(repulse);
			}
			
			return result;
		}
		
		// 按照规则移动 Boid.
		this.move = function() {
			//console.log(acceleration);
			this.velocity_.add(acceleration);
			
			var length = this.velocity_.length();
			if(length > maxSpeed) {
				this.velocity_.divideScalar(length / maxSpeed);
			}
			
			this.position_.add(this.velocity_);
			
			//设置为0;
			acceleration.set(0, 0, 0);
		}
		
		this.run = function(_boids) {
			
			if(this.position_.x > width / 2) {
				//this.position_.x = -width / 2;
				this.velocity_.x = - this.velocity_.x;
			} else if (this.position_.x < - width / 2) {
				//this.position_.x = width / 2;
				this.velocity_.x = - this.velocity_.x;
			}
			
			if(this.position_.y > height / 2) {
				//this.position_.y = -height / 2;
				this.velocity_.y = - this.velocity_.y;
			} else if (this.position_.y < - height / 2) {
				//this.position_.y = height / 2;
				this.velocity_.y = - this.velocity_.y;
			}
		
			if(this.position_.z > depth / 2) {
				//this.position_.z = -depth / 2;
				this.velocity_.z = - this.velocity_.z;
			} else if (this.position_.z < - depth / 2) {
				//this.position_.z = depth / 2;
				this.velocity_.z = - this.velocity_.z;
			}
		
			this.flock(_boids);
			this.move();
		}
}