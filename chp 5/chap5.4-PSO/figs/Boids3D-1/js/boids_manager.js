
//单例模式.
var boidsManager = function() {

	//不对外公开.
	var SCREEN_WIDTH = window.innerWidth,
		SCREEN_HEIGHT = window.innerHeight,
		SCREEN_WIDTH_HALF = SCREEN_WIDTH  / 2,
		SCREEN_HEIGHT_HALF = SCREEN_HEIGHT / 2;

	var camera, scene, renderer,
		birds = [], bird,
		boids = [], boid;
		
	//处理窗口大小发生变化的情况
	function onWindowResize() {
		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();

		renderer.setSize( window.innerWidth, window.innerHeight );
	}

	//对外公开.
	return {
	
		init : function() {
			//初始化摄像机.
			camera = new THREE.PerspectiveCamera(75, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 10000);
			camera.position.z = 450;
	
			scene = new THREE.Scene();
			
			for(var i = 0; i < 200; i++) {
				boid = boids[i] = new Boid();
				
				//Boid的初始位置 x, y, z 的值在-200 ~ 200 之间.
				boid.position_.x = Math.random() * 400 - 200;
				boid.position_.y = Math.random() * 400 - 200;
				boid.position_.z = Math.random() * 400 - 200;
				
				//Boid的初始速度 x, y, z 的值在-1 ~ 1 之间.
				boid.velocity_.x = Math.random() * 2 - 1;
				boid.velocity_.y = Math.random() * 2 - 1;
				boid.velocity_.z = Math.random() * 2 - 1;
				
				boid.setWorldSize(1400, 1000, 1800);
				
				//初始化鸟模型(用于图形显示).
				bird = birds[i] = new THREE.Mesh(new Bird(), new THREE.MeshBasicMaterial( {side: THREE.DoubleSide} ));
				
				bird.phase = Math.floor( Math.random() * 62.83 ); //??
				//加入场景中.
				scene.add(bird);
			}
			
			renderer = new THREE.CanvasRenderer();
			renderer.setClearColor(0x000000);
			renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
			
			document.body.appendChild(renderer.domElement);
			
			window.addEventListener( 'resize', onWindowResize, false );
		},
		
		render : function() {

			for (var i = 0, len = birds.length; i < len; i++) {

				boid = boids[i];
				boid.run(boids);

				bird = birds[i];
				bird.position.copy(boid.position_);

				color = bird.material.color;
				color.r = color.g = color.b = (500 - bird.position.z) / 1000;

				bird.rotation.y = Math.atan2(-boid.velocity_.z, boid.velocity_.x);
				bird.rotation.z = Math.asin(boid.velocity_.y / boid.velocity_.length());
				
				bird.phase = (bird.phase + (Math.max(0, bird.rotation.z) + 0.1)) % 62.83;
				bird.geometry.vertices[5].y = bird.geometry.vertices[4].y = Math.sin(bird.phase) * 5;
			}

			renderer.render(scene, camera);
		}
	};
}();


function animate() {
	requestAnimationFrame(animate);
	boidsManager.render();
}

boidsManager.init();
animate();
