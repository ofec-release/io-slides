%-------------------------------------------------------------------------------------------
\documentclass[aspectratio=169,UTF8,11pt]{beamer}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{colortbl}
\usepackage{color}
\usepackage{booktabs}
\usepackage{threeparttable}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
    \usepackage{graphicx}
\usepackage{animate}

\usepackage{bm}
\usepackage{algorithm2e}
%\usepackage{babel}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\mode<presentation> {
\usetheme{Madrid}
%\setbeamertemplate{footline} % To remove the footer line in all slides uncomment this line
\setbeamertemplate{footline}[frame number] % To replace the footer line in all slides with a simple slide count uncomment this line
\setbeamercolor{page number in head/foot}{fg=blue}
\setbeamertemplate{navigation symbols}{} % To remove the navigation symbols from the bottom of all slides uncomment this line
}

% User Defined Block %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{setspace}
\definecolor{orange}{rgb}{1,0.5,0}
\definecolor{aa}{RGB}{34,139,34}
\definecolor{lightblue}{rgb}{0,0.85,0.9}
\definecolor{darkblue}{rgb}{0,0.7,1}

\definecolor{hanblue}{rgb}{0.27, 0.42, 0.81}
\definecolor{indiagreen}{rgb}{0.07, 0.53, 0.03}
\definecolor{indianred}{rgb}{0.8, 0.36, 0.36}
\definecolor{indianyellow}{rgb}{0.89, 0.66, 0.34}
\definecolor{babypink}{rgb}{0.96, 0.76, 0.76}
\definecolor{ao(english)}{rgb}{0.0, 0.5, 0.0}
\setbeamerfont{block title}{size=\normalsize}
\setbeamerfont{block body}{size=\small}

\newenvironment<>{blueblock}[1]{%
  \setbeamercolor{block title}{fg=white,bg=hanblue}%
  \begin{block}#2{#1}}{\end{block}}

\newenvironment<>{greenblock}[1]{%
  \setstretch{1.3}\setbeamercolor{block title}{fg=white,bg=indiagreen}%
  \begin{block}#2{#1}}{\end{block}}

\newenvironment<>{redblock}[1]{%
  \setstretch{1.3}\setbeamercolor{block title}{fg=white,bg=indianred}%
  \begin{block}#2{#1}}{\end{block}}

\newenvironment<>{yellowblock}[1]{%
  \setstretch{1.3}\setbeamercolor{block title}{fg=white,bg=indianyellow}%
  \begin{block}#2{#1}}{\end{block}}

%----------------------------------------------------------------------------------------
%	PACKAGES
%----------------------------------------------------------------------------------------
\usepackage{graphicx} % Allows including images
%\usepackage{tikz}
%\usetikzlibrary{shapes.geometric, arrows}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title[\textit{Intelligent Optimization}]{Chapter 3 Canonical Optimization Algorithms}
\author[Changhe Li]{Changhe Li} % Your name
\institute[CUG] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
China University of Geosciences (Wuhan)\\
School of Automation\\
%中国地质大学（武汉）自动化学院\\ % Your institution for the title page
\medskip
\textit{lichanghe@cug.edu.cn} % Your email address
}
\date{} % Date, can be changed to a custom date
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}
\maketitle
%\begin{frame}[noframenumbering]           %beamer里重要的概念，每个frame定义一张page
%\centering
%{\large 李长河 \vspace{0.5cm} \\自动化学院710 \vspace{0.5cm}\\ lichanghe@cug.edu.cn}
%\end{frame}

%-----------------------------------------------------------


\addtocounter{framenumber}{-1}
%---------------------------------------------------------------------------------------------
\begin{frame}{Contents}
	\tableofcontents
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Numerical Optimization Algorithms}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
        \tableofcontents[currentsection,hideallsubsections]
\end{frame}

\subsection{Line Search}
\begin{frame}{3.1.1 Line search}
  \begin{blueblock}{Line Search}
    \begin{align}\label{eq:recursive form}
      \bm{x}_{k+1}= \bm{x}_k + \alpha_k\bm{d}_k
      \end{align}
      where $f(\bm{x}_{k+1}) <  f(\bm{x}_k)$ must hold for \alert{minimization}.
  \end{blueblock}
  \begin{greenblock}{Line Search Process}
    \textbf{Step 1:} Set initial solution $\bm{x}_0$ and  counter  $k=0$.

    \textbf{Step 2:}
    \begin{itemize}
      \item Compute a descent direction $\bm{d}_k$ \;
      \item Choose $\alpha_k$ to `loosely' minimize $ f(\bm{x}_{k+1})= f(\bm{x}_k + \alpha_k\bm{d}_k)$ over $\alpha \in \mathbb{R}_+$\;
      \item Update $\bm{x}_{k+1}= \bm{x}_k + \alpha_k\bm{d}_k$ and $k=k+1$\;
    \end{itemize}

    \textbf{Step 3:} If termination is not met, then output $\bm{x}_n$. Otherwise, go to \textbf{Step 2:} .
  \end{greenblock}

\end{frame}


\begin{frame}{3.1.1 Line search}
  \begin{blueblock}{Termination criterion}
    \begin{itemize}
      \item The \textcolor{red}{computing resources} (i.e., time) are exhausted;
      \item The \textcolor{red}{gradient} at $\bm{x}_k$  is smaller than a gradient norm tolerance $\epsilon_g$, i.e., converges at a critical point;
      \item The \textcolor{red}{difference between $\bm{x}_{k+1}$ and  $\bm{x}_{k}$} is smaller than a small constant value of $\epsilon_x$;
      \begin{align}
        \label{eq:termination_difference}
        \begin{aligned}
        \left \|  \bm{x}_{k+1} - \bm{x}_{k} \right \| &< \epsilon_x\\
        \left \|  f(\bm{x}_{k+1}) - f(\bm{x}_{k}) \right \| &< \epsilon_f
        \end{aligned}
        \end{align}

      \item $ f(\bm{x}_{k+1})$ is larger than $f(\bm{x}_{k})$, i.e., \textcolor{red}{the algorithm diverges};
    \end{itemize}
  \end{blueblock}

\end{frame}


\begin{frame}{3.1.1 Line search}
  \begin{columns}
    \begin{column}{0.45\textwidth}
      \begin{figure}[h!t]
        \centering
        \includegraphics[width=\textwidth]{figs/fig3-1-1-1.pdf}
        \caption{Insufficient reduction in $f$}
        \label{fig:wrong_alpha}
        \end{figure}
    \end{column}
    \begin{column}{0.55\textwidth}
      \begin{blueblock}{How to set $\alpha$ to get sufficient reduction in $f$?}
        \begin{itemize}
          \item[(1)] The Armijo condition.\index{A!Armijo condition}
          \begin{equation}
          \label{eq:Armijo}
          f(\bm{x}_k + \alpha_k\bm{d}_k) \leq f(\bm{x}_k) + c_1  \alpha_k \bm{d}_k^{\rm T} \bm{\nabla} f(\bm{x}_k)
          \end{equation}
          \item[(2)] The Wolfe condition.\index{W!Wolfe condition}
          \begin{align}
          \label{eq:Wolfe}
          \begin{aligned}
          f(\bm{x}_k + \alpha_k\bm{d}_k) &\leq f(\bm{x}_k) + c_1 \alpha_k \bm{d}_k^{\rm T} \bm{\nabla} f(\bm{x}_k)   \\
          \nabla f(\bm{x}_k + \alpha_k\bm{d}_k) \bm{d}_k^{\rm T} &\ge  c_2 \bm{d}_k^{\rm T} \bm{\nabla} f(\bm{x}_k)
          \end{aligned}
          \end{align}
          \item[(3)] The strong Wolfe condition.\index{S!strong Wolfe condition}
          \begin{align}
          \label{eq:strong Wolfe}
          \begin{aligned}
          f(\bm{x}_k + \alpha_k\bm{d}_k) &\leq f(\bm{x}_k) + c_1 \alpha_k \bm{d}_k^{\rm T} \bm{\nabla} f(\bm{x}_k)  \\
          \vert \nabla f(\bm{x}_k + \alpha_k\bm{d}_k) \bm{d}_k^{\rm T} \vert &\le  c_2  \vert \bm{\nabla} f(\bm{x}_k) \bm d_k^{\rm T} \vert
          \end{aligned}
          \end{align}
        \end{itemize}
      \end{blueblock}
    \end{column}
  \end{columns}
\end{frame}



\subsection{Steepest Descent Method}
\begin{frame}{3.1.2 Steepest Descent Method}
  \begin{blueblock}{Steepest Descent Method}
  \begin{columns}
  \begin{column}{0.65\textwidth}
    \begin{itemize}
      \item Idea:

      The \textcolor{red}{negative gradient} is the locally steepest descent direction to reduce the objective value.

      \item Principle: \textcolor{red}{the first order Taylor polynomial\index{T!Taylor polynomial}} of $f(\bm{x})$ about $\bm{x}_k$
      \begin{align}\label{eq:first order Taylor 2}
        f(\bm{x})  = f(\bm{x}_k)& +\bm \nabla f(\bm{x}_k)\cdot(\bm{x}-\bm{x}_k)+O(\left \|\bm{x}  -\bm x_{k}\right \|^2)\\
        f(\bm{x}_k+\alpha_k\bm{d}_k)  - f(\bm{x}_k)  &= \alpha_k \bm \nabla f(\bm{x}_k)\cdot\bm{d}_k+\alpha_k^2 O(\left \| 1\right \|)\\
        \bm{d}_k  & = -\frac{\bm\nabla f(\bm{x}_k)}{ \left\| \bm\nabla f(\bm{x}_k) \right\|}
     \end{align}
    \end{itemize}
  \end{column}
  \begin{column}{0.35\textwidth}
  	\begin{figure}
		\centering
  \includegraphics[width=\textwidth]{figs/steepest_decent.png}
	\end{figure}
  \end{column}
  \end{columns}

  


  \end{blueblock}
\end{frame}


\begin{frame}{3.1.2 Steepest Descent Method}

  \begin{columns}
    \begin{column}{0.5\textwidth}

  \begin{yellowblock}{Characteristics}
    \begin{itemize}
      \item Ideally, gradient descent method can achieve a \textcolor{red}{linear convergence rate};
      \item The speed is \textcolor{red}{slow in the slope} near optimum as shown in Figure \ref{fig:zigzag};
      \item \textcolor{red}{Barzilai-Borwein gradient method\index{B!Barzilai-Borwein gradient method}} can speed up the convergence rate:
      \begin{align}\label{eq:Barzilai_Borwein}
        \alpha_k =  \frac{(\bm x_k-\bm x_{k-1})^{\rm T}[\bm \nabla f(\bm{x}_k)- \nabla f(\bm x_{k-1})]}{\left \|\bm  \nabla f(\bm{x}_k)- \nabla f(\bm x_{k-1}) \right \|}
        \end{align}
    \end{itemize}
  \end{yellowblock}
    \end{column}
    \begin{column}{0.45\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{figs/fig3-1-2-1.pdf}
        \caption{ Zigzag steps in steepest descent method}
        \label{fig:zigzag}
        \end{figure}
    \end{column}
  \end{columns}
\end{frame}



\subsection{Newton Method}
\begin{frame}{3.1.3 Newton Method}

  
    \begin{columns}
    \begin{column}{0.6\textwidth}
      \begin{blueblock}{Newton Method}
Suppose that $f(x)$ has a \textcolor{red}{continuous second derivative} at $x_k$, the \textcolor{red}{second order Taylor polynomial\index{T!Taylor polynomial}} of $f(x)$ at $x_k$ is
  \begin{align}\label{eq:second order Taylor 2}
        f(x) \approx f(x_k)+ f'(x_k)(x-x_k)+\frac{1}{2}f''(x_k)(x-x_k)^2 \\
       \text{Then~~~~~~\hspace{2cm}} \min f(x) \Longleftrightarrow f'(x)=0 \\
        f'(x_k)+f''(x_k)(x-x_k) = 0 \\
        \text{The solution is: \hspace{2cm}} x_{k+1}  =x_k - \frac{f'(x_k)}{f''(x_k)} 
      \end{align}
  \end{blueblock}
    \end{column}
    \begin{column}{0.4\textwidth}
      \begin{figure}[h!t]
        \centering
        %\animategraphics[scale=1.5,autoplay]{1}{example4_2/example4_2_}{001}{114}
		\animategraphics[scale=0.2,autoplay]{1}{figs/NewtonIteration_Ani/NewtonIteration_Ani_}{00}{17}
        \end{figure}
    \end{column}
  \end{columns}
  
  \begin{blueblock}{Newton Method in high-dimensional space}
    \begin{align}\label{eq:newton high dimension}\vspace{-1cm}
      \bm x_{k+1} =\bm x_k - H^{-1}(\bm x_k) \nabla f(\bm{x}_k)\\
      H(\bm x_k) =
\begin{bmatrix}
\frac{\partial^2 f(\bm{x}_k)} { \partial x_i \partial x_j}
\end{bmatrix}_{n \times n}
      \end{align}
  \end{blueblock}


\end{frame}


\begin{frame}{3.1.3 Newton Method}

  \begin{columns}
    \begin{column}{0.5\textwidth}

  \begin{yellowblock}{Characteristics}
    \begin{itemize}
      \item Ideally, Newton method can achieve a \textcolor{red}{quadratic convergence rate};
      \item Newton method is \textcolor{red}{sensitive to initial points};
      \item Newton method has \textcolor{red}{strict conditions}: the function must have continuous second derivative and the Hessian matrix must be invertible;
      \item The \textcolor{red}{inverse of the Hessian} takes \textcolor{red}{a lot calculation} while Quasi Newton method overcome this shortcoming;
    \end{itemize}
  \end{yellowblock}
    \end{column}
    \begin{column}{0.45\textwidth}
      \begin{figure}[h!t]
        \centering
        \includegraphics[height=0.36\textheight]{figs/fig3-1-3-1.pdf}\\
        \includegraphics[height=0.36\textheight]{figs/fig3-1-3-2.pdf}
        \caption{Different initial point}
        \label{fig:newton_method}
        \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\subsection{Conjugate Gradient Method}
\begin{frame}{3.1.4 Conjugate Gradient Method}

\begin{center}
\begin{tabular}{cc}
 \includegraphics[height=0.35\textheight,width=0.4\textwidth]{figs/最速下降0.png}&
 \includegraphics[height=0.35\textheight,width=0.4\textwidth]{figs/最速下降1.png}\\
 Gradient decent&Expected\\
 \includegraphics[height=0.35\textheight,width=0.4\textwidth]{figs/最速下降2.png}&
 \includegraphics[height=0.35\textheight,width=0.4\textwidth]{figs/最速下降3.png}\\
 Issue & Solution
\end{tabular}
\end{center}
\end{frame}

\begin{frame}{3.1.4 Conjugate Gradient Method}
  \begin{blueblock}{Conjugate Gradient Method}
    \begin{itemize}
      \item Given the \textcolor{red}{Convex quadratic problem\index{C!convex quadratic problem}}
      \begin{align}\label{eq:quadratic_problem}
      \text{min}_{\bm x\in \mathbb R^n} \: f(\bm x)=\frac{1}{2}\bm x^{\rm T}\bm Q\bm x - \bm b^{\rm T}\bm x,
      \end{align}
    where matrix $\bm Q \in \mathbb R_{n \times n}$ is \textcolor{red}{symmetric} and \textcolor{red}{positive definite};
     \item  This problem is equivalent to $\bm Q \bm x=\bm b$;
    \item The idea is to  find an optimal search direction $d_k, k=1,2,\ldots, n$, which is \textcolor{red}{Q-orthogonal} to all previous search directions.

    \item Since $\left \{\bm d_1,\bm d_2,\cdots,\bm d_n\right\}$ vectors are independent, then
     \begin{align}\label{eq:conjugate_deduce_1}
\bm x^*   = \alpha_1 \bm d_1 + \cdots+ \alpha_{n}\bm d_{n}
\end{align}
    where $\bm x^*$ denote the optimal solution


    \end{itemize}
  \end{blueblock}
\end{frame}

\begin{frame}{3.1.4 Conjugate Gradient Method}
  \begin{blueblock}{Conjugate Gradient Method}
    \begin{itemize}

     \item  Therefore
\begin{align}\label{eq:conjugate_deduce_2}
\bm d_i^{\rm T} \bm Q \bm x^*   =  \bm d_i^{\rm T} \bm Q (\alpha_1 \bm d_1 + \cdots + \alpha_{n}\bm d_{n}) = \bm d_i^{\rm T} \alpha_i\bm Q\bm d_i
\end{align}
\item Then
\begin{align}\label{eq:conjugate_deduce_3}
\alpha_i = \frac{\bm d_i^{\rm T}\bm Q \bm x^*}{\bm d_i^{\rm T}\bm Q \bm d_i} = \frac{\bm d_i^{\rm T}\bm b}{\bm d_i^{\rm T}\bm Q\bm d_i}
\end{align}
\item By the update rule $\bm x_{k+1} = \bm x_{k} + \alpha_k \bm{d}_k$
    \begin{align}\label{eq:conjugate}
      \alpha_k = - \frac{\bm{d}_k^{\rm T} \bm g_k}{\bm{d}_k^{\rm T} \bm Q \bm{d}_k} ,& \bm d_{k+1} = - \bm g_{k+1}+ \frac{\bm g^{\rm T}_{k+1}\bm Q \bm d_k}{\bm{d}_k^T \bm Q \bm{d}_k} \bm{d}_k
      \end{align}
      where $\bm{g}_k=\nabla f(\bm {x}_k), \bm d_{1} = -\bm g_1 = \bm b-\bm Q \bm x_1$.
    \end{itemize}
  \end{blueblock}
\end{frame}



\begin{frame}{3.1.4 Conjugate Gradient Method}


  \begin{yellowblock}{Characteristics}

    \begin{itemize}
      \item It needs the \textcolor{red}{first derivative information} while has fast convergence speed;
      \item  It is one of the most useful methods to \textcolor{red}{solve large-scale system} of linear equations;
      \item It is still \textcolor{red}{sensitive to initial points} and cannot avoid being \textcolor{red}{trapped into local optima}.
    \end{itemize}
  \end{yellowblock}

\end{frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{State Space Search} \label{sec 3.2} \index{S!state space search}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
        \tableofcontents[currentsection,hideallsubsections]
\end{frame}

\begin{frame}{3.2 State Space Search}

  \begin{blueblock}{State space search}
    \begin{itemize}
      \item Solve discrete optimization problems
      \item Modelled as
      \[\mathbf{S} =\left (\mathbb{S}, \mathbb A, action(s),result(s,a), cost(s,a) \right)\] where
      \begin{itemize}
      \item $\mathbb S$ is the set of all possible \alert{states}
      \item $\mathbb A$ is the set of all possible \alert{actions}
      \item $action(s)$ is the set of all \alert{feasible actions} of the state $s$
      \item $result(s,a)$ is the \alert{next state} after performing  action $a$ at state $s$
      \item $cost(a,s)$ returns the \alert{cost} by taking action $a$ at state $s$
      \end{itemize}
      \end{itemize}
  \end{blueblock}

\end{frame}

\begin{frame}{3.2 State Space Search}

  \begin{columns}
    \begin{column}{0.5\textwidth}

  \begin{blueblock}{Measurements of the performance}
    \begin{itemize}
      \item[(1)] Completeness: whether the algorithm can \textcolor{red}{find a goal state} when there is at least one goal states.
      \item[(2)] Optimality: whether the algorithm guarantees to \textcolor{red}{find the optimal solution};
      \item[(3)] Space complexity: the \textcolor{red}{maximum space} it takes to solve the problem;
      \item[(4)] Time complexity: the \textcolor{red}{time} it takes to solve the problem at the worst case;
      \end{itemize}
  \end{blueblock}
    \end{column}
    \begin{column}{0.45\textwidth}
      \begin{figure}[h!t]
        \centering
        \includegraphics[width= 0.8\textwidth]{figs/fig3-2-1-1.pdf}
        \caption{State space search}
        \label{fig:3-2-1-1}
        \end{figure}

    \end{column}
  \end{columns}
\end{frame}


\subsection{State Space}
\begin{frame}{3.2.1.1 The Shortest Path Problem}

  \begin{columns}
    \begin{column}{0.5\textwidth}

  \begin{blueblock}{The formulation of the shortest path problem}
    \begin{itemize}
      \item[(1)] \textcolor{red}{Solution $\mathbf{x}$}: the solution to this problem is any path from $\mathtt{Jilin}$ to $\mathtt{Hubei}$
      \item[(2)] \textcolor{red}{State $s$}: the initial state $s_0 = \mathtt{Jilin} $ and the goal state $s_g =  \mathtt{Hubei}$.
      \item[(3)] \textcolor{red}{Action $a$}: a road that connect two cities $s_i$ and $s_j$ is written as $a_{s_i}^{s_j}$, e.g.,  $a_{\mathtt{Hebei}}^{\mathtt{Shangdong}}$.
    \end{itemize}
  \end{blueblock}
  \end{column}

  \begin{column}{0.45\textwidth}
      \begin{figure}[h!t]
        \centering
        \includegraphics[height= 0.85\textheight]{figs/fig3-2-1-3.pdf}
       % \caption{Map of the part of China}
        \label{fig:3-2-1-3-1}
        \end{figure}

    \end{column}
  \end{columns}
\end{frame}


\begin{frame}{3.2.1.2 The Shortest Path Problem}

  \begin{columns}
    \begin{column}{0.55\textwidth}

  \begin{blueblock}{The formulation of the shortest path problem}

    \begin{itemize}

      \item[(4)] \textcolor{red}{$action(s)$}: there are three roads that connect the city $Hebei$, then $action( \mathtt{Hebei}) = \{a_{\mathtt{Hebei}}^{\mathtt{Liaoning}}, a_{\mathtt{Hebei}}^{\mathtt{Shangdong}}, $ $a_{\mathtt{Hebei}}^{\mathtt{Shanxi}}\}$.
      \item[(5)] \textcolor{red}{$result(s,a)$}: $result ( \mathtt{Hebei}, a_{\mathtt{Hebei}}^{\mathtt{Shangdong}} ) = \mathtt{Shangdong}$.
      \item[(6)] \textcolor{red}{$cost(s,a)$}:  $cost ( \mathtt{Hebei}, a_{\mathtt{Hebei}}^{\mathtt{Shangdong}} ) = 85$.

    \end{itemize}

  \end{blueblock}
    \end{column}
    \begin{column}{0.4\textwidth}
      \begin{figure}[h!t]
        \centering
        \includegraphics[height= 0.8\textheight]{figs/fig3-2-1-3.pdf}
       % \caption{Map of the part of China}
        \label{fig:3-2-1-3-2}
        \end{figure}

    \end{column}
  \end{columns}
\end{frame}


\begin{frame}{3.2.1 State Space}

  \begin{blueblock}{Classicale state space search}
    \begin{itemize}
      \item[(1)] \textbf{Uninformed search} does \textcolor{red}{not use any information} about the goal state:

      breadth-first search, depth-first search, and depth limited search.
      \item[(2)] \textbf{Informed search} makes use of \textcolor{red}{the problem-domain knowledge} to reach the goal state \textcolor{red}{faster} and find a \textcolor{red}{better} solution:

      greedy search,  A$^*$ search, and Monte-Carlo tree search.
      \end{itemize}
  \end{blueblock}
\end{frame}

\subsection{Uniformed Search}
\begin{frame}{3.2.2.1 Breadth-First Search (BFS)}

  \begin{columns}

    \begin{column}{0.6\textwidth}
      \begin{figure}[h!t]
        \centering
        \includegraphics{figs/fig3-2-1-4.pdf}
        \caption{The search order of breadth-first search in a search tree}
        \label{fig:3-2-1-4}
        \end{figure}
    \end{column}

    \begin{column}{0.4\textwidth}

  \begin{blueblock}{Performance of BFS}
    \begin{itemize}
      \item[(1)] Completeness: BFS \textcolor{red}{can} always find a feasible solution;
      \item[(2)] Optimality: BFS does \textcolor{red}{not} guarantee to find the optimal solution;
      \item[(3)] Space complexity: In the worst case, the space complexity of BFS is \textcolor{red}{$O(\|\mathbb S\|)$}  for explicit search tree;
      \item[(4)] Time complexity: Time complexity of BFS is \textcolor{red}{$O(\|\mathbb S\| + \|\mathbb A\|)$}  for exploring every node and each edge.
      \end{itemize}
  \end{blueblock}
\end{column}

  \end{columns}
\end{frame}



\begin{frame}{3.2.2.2 Depth-First Search (DFS)}

  \begin{columns}

    \begin{column}{0.6\textwidth}
      \begin{figure}[h!t]
        \centering
        \includegraphics[width= 0.86\textwidth]{figs/fig3-2-1-5.pdf}
        \caption{ The search order of depth-first search in a search tree}
        \label{fig3-2-1-5}
        \end{figure}
    \end{column}

    \begin{column}{0.4\textwidth}

  \begin{blueblock}{Performance of DFS}
    \begin{itemize}
      \item[(1)] Completeness. DFS \textcolor{red}{guarantees} to find a feasible solutionm.
      \item[(2)] Optimality. DFS does \textcolor{red}{not} guarantee to find the optimal solution.
      \item[(3)] Space complexity: \textcolor{red}{$O( \| \mathbb{S}\|)$} for the explicit search tree.
      \item[(4)] Time complexity: \textcolor{red}{$O( \| \mathbb{S}\|+\|\mathbb{A}\|)$} for expanding the whole search tree.

      \end{itemize}
  \end{blueblock}
\end{column}
  \end{columns}
\end{frame}


\begin{frame}{3.2.2.3 Depth-Limited Search (DLS)}

  \begin{columns}

    \begin{column}{0.6\textwidth}
      \begin{figure}[h!t]
        \centering
        \includegraphics[width= 0.9\textwidth]{figs/fig3-2-1-6.pdf}
        \caption{ The search order of Depth-limited Search in a search tree and it only visited the states with lower depth than the limited depth}
        \label{fig3-2-1-6}
        \end{figure}
    \end{column}

    \begin{column}{0.4\textwidth}

  \begin{blueblock}{Performance of DFS}
    \begin{itemize}
      \item[(1)] Completeness. Depth-limited search can \textcolor{red}{not} find a feasible solution when the limited depth $\mathtt{L}$ is smaller than the depth of any feasible state.
      \item[(2)] Optimality. Depth-limited search also does \textcolor{red}{not} guarantee to find the optimal solution.
      \item[(3)] Space complexity: \textcolor{red}{$O( \| \mathbb{S}\|)$}
      \item[(4)] Time complexity: \textcolor{red}{$O( \| \mathbb{S}\|+\|\mathbb{A}\|)$}
      \end{itemize}
  \end{blueblock}
\end{column}
  \end{columns}
\end{frame}

\subsection{Informed Search}
\begin{frame}{3.2.3.1 Greedy Search}
  \begin{blueblock}{Basic idea}
   Greedy search always choose the next state with minimum cost at current state.
  \end{blueblock}
  \begin{columns}
    \begin{column}{0.65\textwidth}
      \begin{figure}
        \centering
        \includegraphics[height= 0.6\textheight]{figs/fig3-2-2-1.pdf}
        \caption{Search order of greedy search in a shortest path problem}
        \label{fig3-2-2-1}
        \end{figure}
    \end{column}

    \begin{column}{0.35\textwidth}


      \begin{blueblock}{Performance of Greedy search}
        \begin{itemize}
          \item[(1)] Completeness: greedy search \textcolor{red}{guarantees} to find the goal state with the search strategy.
          \item[(2)] Optimality: it may guide the search to select a \textcolor{red}{bad action} and then gets a \textcolor{red}{bad solution}.
          \item[(3)] Space complexity: \textcolor{red}{$O( \| \mathbb{S}\|)$}.
          \item[(4)] Time complexity: \textcolor{red}{$O( \| \mathbb{S}\|+\|\mathbb{A}\|)$}.
          \end{itemize}

      \end{blueblock}
    \end{column}

  \end{columns}
\end{frame}


\begin{frame}{3.2.3.2 A$^*$ Search}

  \begin{blueblock}{Idea of A$^*$ Search}

    A$^*$ search is to select  the next state along which the path has the \textcolor{red}{minimum expected cost} from the initial state to the goal state.
    \[f(s)=g(s)+h(s)\]

  \end{blueblock}

    \begin{blueblock}{Performance of A$^*$ Search}
      \begin{itemize}
        \item[(1)] Completeness: it \textcolor{red}{guarantees} to find the goal state with the search strategy.
        \item[(2)] Optimality: it \textcolor{red}{can} find the global optimal when the designed heuristic function satisfies the \textcolor{red}{admissibility constraint} as follows
        \begin{align}
        0 \leq h (s) \leq h^*(s)
        \end{align}
        where $h^*(s)$ is the true optimal cost from the current state $s$ to the goal state.
        \item[(3)] Space complexity: \textcolor{red}{$O( \| \mathbb{S}\|)$}. Time complexity: \textcolor{red}{$O( \| \mathbb{S}\|+\|\mathbb{A}\|)$}.
        \end{itemize}

    \end{blueblock}

\end{frame}


\begin{frame}{3.2.3.3 Monte-Carlo Tree Search}
\begin{blueblock}{Basic idea}
Combine the random simulation and tree search, useful for chess and Go games. It has four components:
\end{blueblock}
  \begin{figure}[h!t]
    \centering
    \includegraphics[scale=0.95]{figs/fig3-2-2-2.pdf}
    \caption{Framework of Monte-Carlo tree search}
    \label{fig3-2-2-2}
    \end{figure}
\end{frame}


\begin{frame}{3.2.3.3 Monte-Carlo Tree Search}
      \begin{blueblock}{Characteristics of Monte-Carlo tree search}
        \begin{itemize}
          \item Advantages:   \begin{itemize}
            \item[(1)] With simulation, it does \textcolor{red}{not need any heuristic information} to guide the search or  an explicit evaluation function to evaluate each state.
            \item[(2)] It tends to search the \textcolor{red}{more promising subtree}, which helps to solve the complex problem with many states and many actions.
            \item[(3)] It is clear and simple so it can be \textcolor{red}{easily implemented}.
            \item[(4)] The Monte-Carlo tree search can be \textcolor{red}{terminated at any time} with the best solution it found.
            \end{itemize}
          \item Disadvantages:
            \begin{itemize}
            \item[(1)] Monte-Carlo tree search may \textcolor{red}{miss some critical state} and thus lead to \textcolor{red}{failure} in game problems.
            \item[(2)] It may need \textcolor{red}{many samplings} to get a relatively accurate evaluation, which is time-consuming.
            \end{itemize}
          \end{itemize}
      \end{blueblock}

\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Single-solution-based Random Search } \label{sec 3.3}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
        \tableofcontents[currentsection,hideallsubsections]
\end{frame}


%\subsection{Hill Climbing}
%\begin{frame}{3.3.1 Hill Climbing}\index{H!hill climbing}
%
%  \begin{columns}
%    \begin{column}{0.5\textwidth}
%
%      \begin{greenblock}{Hill climbing process}
%        \textbf{Step 1:} Insert initial solution $\bm{x}$;
%
%        \textbf{Step 2:}
%        \begin{itemize}
%          \item Using $\bm{x}_{best}$ to create a new solution $\bm{x}$, evaluate its objective value $f(\bm{x})$\;
%          \item If $f(\bm{x})$ is better than $f(\bm{x}_{best})$, $\bm{x}_{best}$ $\leftarrow$ $\bm{x}$\;
%        \end{itemize}
%
%        \textbf{Step 3:} If termination is not met, then output $\bm{x}$. Otherwise, go to \textbf{Step 2:} .
%      \end{greenblock}
%\end{column}
%
%\begin{column}{0.5\textwidth}
%  \begin{figure}[h!t]
%    \centering
%    \includegraphics{figs/fig3-3-1-1.pdf}
%    %\includegraphics[scale=0.24]{figs/fig3-3-1-2}
%    \caption{Diagram of the hill climbing}\label{fig3-3-1-1}
%    \end{figure}
%\end{column}
%  \end{columns}
%\end{frame}


\subsection{Simulated Annealing Algorithm}
\begin{frame}{3.3.2 Simulated Annealing Algorithm (SA)}

  \begin{greenblock}{Barrier to local search}
\begin{center}
    \includegraphics[scale=0.6]{figs/barrier.png}
  \end{center}
  \end{greenblock}

\end{frame}


\begin{frame}{3.3.2 Simulated Annealing Algorithm (SA)}

  \begin{greenblock}{The annealing process of heated solids}
  By allowing occasional ascent in the search process, we might be able to escape the trap of local minima.
  \end{greenblock}
\begin{center}
    \includegraphics[scale=0.6]{figs/SA.png}
  \end{center}
\end{frame}

\begin{frame}{3.3.2 Simulated Annealing Algorithm (SA)}

  \begin{blueblock}{Idea of SA}

    SA mimics the \textcolor{red}{heating and controlled cooling process} of metals with the aim of increasing the ductility and reducing the hardness.

    SA always accepts new solutions that are better than the current solution, but with a certain probability to \textcolor{red}{select inferior solutions}
    that have poorer objective values.
  \end{blueblock}


  \begin{blueblock}{Probability of accepting an inferior solution}
    \begin{align}\label{}
      p(\Delta E)=\exp \left(\frac{-\Delta E}{kT}\right)
      \end{align}
      where $\Delta E$ is  the difference in value/cost between states, $k$ is the Boltzmann constant\index{B!Boltzmann constant}.
  \end{blueblock}
\end{frame}


\begin{frame}{3.3.2 Simulated Annealing Algorithm (SA)}
\scalebox{0.9}{
\begin{algorithm}[H]
\KwIn{Initial solution $\bm{x}$}
\KwOut{Final solution $\bm{x}_{best}$}
$\bm{x}_{cur} \leftarrow \bm{x}$; $\bm{x}_{best} \leftarrow \bm{x}$; $t$=0\;
\While{termination criterion is not fulfilled}{
Compute the energy difference $\Delta E=f(\bm{x})-f(\bm{x}_{cur})$\;
\If{$\Delta E \leq 0$}{
$\bm{x}_{cur} \leftarrow \bm{x}$\;
\lIf{$f(\bm{x}_{cur})<f(\bm{x}_{best})$}{
$\bm{x}_{best} \leftarrow \bm{x}_{cur}$
}}
\Else{
$T$ $\leftarrow$ getTemperature($t$); \tcp{used in the calculation of $p(\Delta E)$}
\lIf{$rand<p(\Delta E)$}{
	$\bm{x}_{cur} \leftarrow \bm{x}$
}
}
Using $\bm{x}_{cur}$ to create a new solution $\bm{x}$, evaluate its objective value $f(\bm{x})$\;
$t=t+1$.
}
return $\bm{x}_{best}$ and $f(\bm{x}_{best})$.
\end{algorithm}}
\end{frame}


\begin{frame}{3.3.2 Simulated Annealing Algorithm (SA)}

  \begin{greenblock}{Simulated annealing process}
Acceptance of new state is a function of temperature and how good/bad the state is compared to current one. Relatively good states (low-delta) are almost always accepted
  \end{greenblock}
  \begin{center}
     \begin{tabular}{ccc}
    \includegraphics[scale=0.27]{figs/delta1.png}& \includegraphics[scale=0.27]{figs/delta2.png}& \includegraphics[scale=0.27]{figs/delta3.png}
  \end{tabular}
  \end{center}

\end{frame}

%\subsection{Iterated Local Search}
%
%\begin{frame}{3.3.3 Iterated Local Search}\index{I!iterated local search}
%  \begin{columns}
%    \begin{column}{0.5\textwidth}
%      \begin{greenblock}{Iterated local search process}
%        \textbf{Step 1:} Insert initial solution $\bm{x}$;
%
%        \textbf{Step 2:}
%        \begin{itemize}
%          \item $\bm{x}'$$\leftarrow$Perturbation($\bm{x}$)\;
%          \item $\bm{x}''$$\leftarrow$LocalSearch($\bm{x}'$)\;
%          \item $\bm{x}$$\leftarrow$Apply Acceptance Criterion($\bm{x}$, $\bm{x}''$, history)\;
%        \end{itemize}
%
%        \textbf{Step 3:} If termination is not met, then output $\bm{x}$. Otherwise, go to \textbf{Step 2:} .
%      \end{greenblock}
%\end{column}
%
%\begin{column}{0.5\textwidth}
%
%  \begin{figure}[h!t]
%    \centering
%    \includegraphics{figs/fig3-3-2-1.pdf}
%    \caption{Diagram of the iterated local search}\label{fig3-3-2-1}
%    \end{figure}
%\end{column}
%  \end{columns}
%\end{frame}
%
%\subsection{Variable Neighborhood Search}
%\begin{frame}{3.3.4 Variable Neighborhood Search (VNS)}
%
%  \begin{blueblock}{Idea of VNS}
%    VNS changes the neighborhood both within a \textcolor{red}{local search phase} to find a local optimum,
%    and in a \textcolor{red}{perturbation phase} to get out of the local optima.
%
%
%    VNS explores increasingly \textcolor{red}{distant neighborhoods} of the current solution and move to a new location if and only if an improvement was made.
%  \end{blueblock}
%
%\end{frame}
%
%
%\begin{frame}{3.3.4 Variable Neighborhood Search (VNS)}
%
%  \begin{greenblock}{Variable neighborhood search}
%    \textbf{Step 1:} Input a set of neighborhood structures $N_k$ for shaking,
%    and set $k$=1\;
%
%    \textbf{Step 2:}
%    While $k<k_{\max}$ do
%    \begin{itemize}
%      \item Pick a random solution $\bm{x}'$ from the neighborhood $N_k$ of $\bm{x}$\;
%      \item  Apply some local search method with $\bm{x}'$  with the result of $\bm{x}''$ \;
%      \item If $f(\bm{x}'')$ is better than $f(\bm{x})$
%      \begin{itemize}
%        \item $\bm{x} \leftarrow \bm{x}''$, and continue the search with $N_{k+1}$\;
%      \end{itemize}
%      \item Else
%      \begin{itemize}
%        \item $k=k+1$.
%      \end{itemize}
%    \end{itemize}
%
%    \textbf{Step 3:} If termination is not fullfilled, then output $\bm{x}$. Otherwise, go to \textbf{Step 2:}
%  \end{greenblock}
%
%\end{frame}


\begin{frame}
\centering
\huge
Thank you!\\
Q\&A
\end{frame}




\end{document}
