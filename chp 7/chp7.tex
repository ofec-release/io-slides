%-------------------------------------------------------------------------------------------
\documentclass[aspectratio=169,UTF8,11pt]{beamer}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{colortbl}
\usepackage{color}
\usepackage{booktabs}
\usepackage{threeparttable}
\usepackage{hyperref}
%\usepackage{babel}
\usepackage[style=british]{csquotes}
\usefonttheme[onlymath]{serif}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\mode<presentation> {
\usetheme{Madrid}
%\setbeamertemplate{footline} % To remove the footer line in all slides uncomment this line
\setbeamertemplate{footline}[frame number] % To replace the footer line in all slides with a simple slide count uncomment this line
\setbeamercolor{page number in head/foot}{fg=blue}
\setbeamertemplate{navigation symbols}{} % To remove the navigation symbols from the bottom of all slides uncomment this line
}

% User Defined Block %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{setspace}
\definecolor{orange}{rgb}{1,0.5,0}
\definecolor{aa}{RGB}{34,139,34}
\definecolor{lightblue}{rgb}{0,0.85,0.9}
\definecolor{darkblue}{rgb}{0,0.7,1}

\definecolor{hanblue}{rgb}{0.27, 0.42, 0.81}
\definecolor{indiagreen}{rgb}{0.07, 0.53, 0.03}
\definecolor{indianred}{rgb}{0.8, 0.36, 0.36}
\definecolor{indianyellow}{rgb}{0.89, 0.66, 0.34}
\definecolor{babypink}{rgb}{0.96, 0.76, 0.76}
\definecolor{ao(english)}{rgb}{0.0, 0.5, 0.0}
\setbeamerfont{block title}{size=\normalsize}
\setbeamerfont{block body}{size=\small}

\newenvironment<>{blueblock}[1]{%
  \setbeamercolor{block title}{fg=white,bg=hanblue}%
  \begin{block}#2{#1}}{\end{block}}

\newenvironment<>{greenblock}[1]{%
  \setstretch{1.3}\setbeamercolor{block title}{fg=white,bg=indiagreen}%
  \begin{block}#2{#1}}{\end{block}}

\newenvironment<>{redblock}[1]{%
  \setstretch{1.3}\setbeamercolor{block title}{fg=white,bg=indianred}%
  \begin{block}#2{#1}}{\end{block}}

\newenvironment<>{yellowblock}[1]{%
  \setstretch{1.3}\setbeamercolor{block title}{fg=white,bg=indianyellow}%
  \begin{block}#2{#1}}{\end{block}}

\def\signed #1{{\leavevmode\unskip\nobreak\hfil\penalty50\hskip1em
\hbox{}\nobreak\hfill #1%
\parfillskip=0pt \finalhyphendemerits=0 \endgraf}}

\newsavebox\mybox
\newenvironment{aquote}[1]
  {\savebox\mybox{#1}\begin{quote}\openautoquote\hspace*{-.7ex}}
  {\unskip\closeautoquote\vspace*{1mm}\signed{\usebox\mybox}\end{quote}}

%----------------------------------------------------------------------------------------
%	PACKAGES
%----------------------------------------------------------------------------------------
\usepackage{graphicx} % Allows including images
%\usepackage{tikz}
%\usetikzlibrary{shapes.geometric, arrows}
\usepackage{listings}
\lstset{language=C++,
    columns=flexible,
   % basicstyle=\scriptsize\ttfamily,                                      % 设定代码字体、大小4
    basicstyle=\footnotesize\ttfamily,
    %numbers=left,xleftmargin=2em,framexleftmargin=2em,                   % 在左侧显示行号
    %numberstyle=\color{darkgray},                                        % 设定行号格式
    keywordstyle=\color{blue},                                            % 设定关键字格式
    commentstyle=\color{ao(english)},                                     % 设置代码注释的格式
    stringstyle=\color{brown},                                            % 设置字符串格式
    %showstringspaces=false,                                              % 控制是否显示空格
	%frame=lines,                                                         % 控制外框
    breaklines,                                                           % 控制是否折行
    postbreak=\space,                                                     % 控制折行后显示的标识字符
    breakindent=5pt,                                                      % 控制折行后缩进数量
    emph={size\_t,array,deque,list,map,queue,set,stack,vector,string,pair,tuple}, % 非内置类型
    emphstyle={\color{teal}},
    escapeinside={(*@}{@*)},
}
%---------------------------------------------------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title[\textit{Intelligent Optimization}]{Chapter 7 Exploitation versus Exploration}
\author[Changhe Li]{Changhe Li} % Your name
\institute[CUG] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
China University of Geosciences (Wuhan)\\
School of Automation\\
%中国地质大学（武汉）自动化学院\\ % Your institution for the title page
\medskip
\textit{lichanghe@cug.edu.cn} % Your email address
}
\date{} % Date, can be changed to a custom date
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}
\maketitle
%\begin{frame}[noframenumbering]           %beamer里重要的概念，每个frame定义一张page
%\centering
%{\large 李长河 \vspace{0.5cm} \\自动化学院710 \vspace{0.5cm}\\ lichanghe@cug.edu.cn}
%\end{frame}

%-----------------------------------------------------------


\addtocounter{framenumber}{-1}
%---------------------------------------------------------------------------------------------
\begin{frame}{Contents}
  % \tableofcontents
  \begin{columns}[onlytextwidth,T]
    \begin{column}{.5\textwidth}
        \tableofcontents[sections={1-3}]
    \end{column}
    \begin{column}{.5\textwidth}
        \tableofcontents[sections={4-}]
    \end{column}
  \end{columns}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{7.1 Introduction}
  \begin{aquote}{Eiben, A. E. and Schippers, C. A.}
    Exploration and exploitation are the two cornerstones of problem solving by search.
  \end{aquote}
  \pause
  \begin{columns}[onlytextwidth]
    \begin{column}{0.55\textwidth}
      \begin{blueblock}{Terminology}
        \begin{description}
          \item[Exploiation] search \alert{around the best point} found so far
          \item[Exploration] search in \alert{rarely-visited} areas
        \end{description}
      \end{blueblock}
      \begin{yellowblock}{Pros and Cons}
        \begin{description}
          \item[Exploiation] easy to make improvement but hard to escape local optimum
          \item[Exploration] easy to escape local optimum but hard to find better solutions
        \end{description}
      \end{yellowblock}
    \end{column}
    \begin{column}{0.4\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{figs/fig7-1-1.pdf}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Exploitation and Exploration Methods}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \tableofcontents[currentsection,hideallsubsections]
\end{frame}

\subsection{Iterative Methods}

\begin{frame}{7.2.1 Iterative Methods}
  \begin{columns}[onlytextwidth]
    \begin{column}{0.5\textwidth}
      \begin{greenblock}{Examples}
        \begin{itemize}
          \item Gradient method
          \item Newton method
        \end{itemize}
      \end{greenblock}
    \end{column}
    \begin{column}{0.46\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{figs/fig7-2-1.pdf}
      \end{figure}
    \end{column}
  \end{columns}
  \begin{yellowblock}{Pros and Cons}
    \begin{description}
      \item[Cons] only have the ability to exploit
    \end{description}
  \end{yellowblock}
\end{frame}

\subsection{Single-solution Meta-heuristics}

\begin{frame}{7.2.2 Single-solution Meta-heuristics}
  \begin{columns}[onlytextwidth]
    \begin{column}{0.5\textwidth}
      \begin{greenblock}{Examples}
        \begin{itemize}
          \item Stochastic hill climbing
          \item Simulated annealing
        \end{itemize}
      \end{greenblock}
    \end{column}
    \begin{column}{0.46\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{figs/fig7-2-2.pdf}
      \end{figure}
    \end{column}
  \end{columns}
  \begin{yellowblock}{Pros and Cons}
    \begin{description}
      \item[Pros] explore regions far away from the initial point in early stages
      \item[Cons] gradually lose its exploration ability
    \end{description}
  \end{yellowblock}
\end{frame}

\subsection{Population-based Meta-heuristics}

\begin{frame}{7.2.3 Population-based Meta-heuristics}
  \begin{columns}[onlytextwidth]
    \begin{column}{0.5\textwidth}
      \begin{greenblock}{Examples}
        \begin{itemize}
          \item Variant evolutionary algorithms (GA, ES, DE, PSO ,etc.)
        \end{itemize}
      \end{greenblock}
    \end{column}
    \begin{column}{0.46\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{figs/fig7-2-3.pdf}
      \end{figure}
    \end{column}
  \end{columns}
  \begin{yellowblock}{Pros and Cons}
    \begin{description}
      \item[Pros] much more stronger exploration ability and exploitation ability
      \item[Cons] whether to explore or exploit depends on the tendency of population is shrinking or not
    \end{description}
  \end{yellowblock}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Enhancing Exploration and Exploitation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \tableofcontents[currentsection,hideallsubsections]
\end{frame}

\subsection{Exploration Enhancement Methods}

\begin{frame}{7.3.1 Exploration Enhancement Methods}
  \begin{blueblock}{\textbf{Intervened reproduction} --- Parameters control}
    \begin{itemize}
      \item Adjust reproduction parameters to \alert{increase the search range} around the parental individuals
    \end{itemize}
  \end{blueblock}
  \begin{table}[h]
    \centering
    \fbox{\scriptsize%
    \begin{tabular}{c|c}
    \textbf{Algorithm Name} & \textbf{Reproduction parameters} \\\hline
    GA & Crossover probability, mutation probability\\\hline
    DE & Mutation factor $F$, crossover probability $Cr$\\\hline
    PSO & Inertia weight $W$, accelerators $C_1$ $C_2$
    \end{tabular}}
  \end{table}
  \begin{greenblock}{Example}
    \begin{itemize}
      \item Increase $F$ and $Cr$ in DE to generate offspring far away from the parents
    \end{itemize}
  \end{greenblock}
\end{frame}

\begin{frame}{7.3.1 Exploration Enhancement Methods}
  \begin{columns}[onlytextwidth]
    \begin{column}{0.6\textwidth}
      \begin{blueblock}{\textbf{Intervened reproduction} --- Restricted mating}
        \begin{itemize}
          \item Select \alert{parents} that are \alert{farther apart} to generate offspring in a much wider area
        \end{itemize}
      \end{blueblock}
      \begin{greenblock}{Example}
        \begin{enumerate}
          \item Restrict $p_2$, $p_3$, $p_4$, $p_5$ from being selected simultaneously
          \item $p_2$ will more likely to mate with distant individuals such as $p_1$, $p_7$,  $p_{9}$, $p_{10}$
        \end{enumerate}
      \end{greenblock}
    \end{column}
    \begin{column}{0.36\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{figs/fig7-3-1.pdf}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{7.3.1 Exploration Enhancement Methods}
  \begin{columns}[onlytextwidth]
    \begin{column}{0.6\textwidth}
      \begin{blueblock}{\textbf{Intervened competition} --- Restricted competition}
        \begin{itemize}
          \item Make the \alert{competitions} only \alert{inside the neighborhood} of each individual
        \end{itemize}
      \end{blueblock}
      \begin{greenblock}{Example}
        \begin{itemize}
          \item $p_{10}$ will not be eliminated by $p_2$
        \end{itemize}
      \end{greenblock}
      \begin{blueblock}<2->{\textbf{Intervened competition} ---  Fitness modification}
        \begin{itemize}
          \item \alert{Decrease the fitness values} of areas around the frequently visited points
        \end{itemize}
      \end{blueblock}
    \end{column}
    \begin{column}{0.36\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{figs/fig7-3-1.pdf}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{7.3.1 Exploration Enhancement Methods}
  \begin{blueblock}{\textbf{Irrigation} --- Random immigration}
    \begin{itemize}
      \item \alert{Replace} one or more individuals with some \alert{randomly initialized} individuals
    \end{itemize}
  \end{blueblock}
  \begin{blueblock}<2->{\textbf{Irrigation} --- Archive solution}
    \begin{itemize}
      \item Store some \alert{earlier-stage} individuals and make them \alert{participate in the current} reproduction
    \end{itemize}
  \end{blueblock}
  \begin{blueblock}<3->{\textbf{Multi-population} --- Overlapping multi-population}
    \begin{enumerate}
      \item Make \alert{several} populations search \alert{simultaneously}
      \item Occasionally \alert{interact them} with each other
    \end{enumerate}
  \end{blueblock}
\end{frame}

\subsection{Exploitation Enhancement Methods}

\begin{frame}{7.3.2 Exploitation Enhancement Methods}
  \begin{blueblock}{\textbf{Intervened reproduction} --- Parameters control}
    \begin{itemize}
      \item Adjust reproduction parameters to \alert{decrease the mutation step}
    \end{itemize}
  \end{blueblock}
  \begin{blueblock}<2->{\textbf{Intervened reproduction} --- Inbreeding mating}
    \begin{itemize}
      \item Choose \alert{neighboring} individuals as \alert{parents}
    \end{itemize}
  \end{blueblock}
  \begin{blueblock}<3->{\textbf{Memetic algorithm}}
    \begin{itemize}
      \item Combination of population-based EA and single solution optimizers
      \begin{itemize}
        \item Population is used for enhancing the diversity
        \item \alert{Single solution optimizers} are used to \alert{enhance local search}
      \end{itemize}
    \end{itemize}
  \end{blueblock}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Balancing Exploration and Exploitation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \tableofcontents[currentsection,hideallsubsections]
\end{frame}

\subsection{Explicit Differentiation Methods}

\begin{frame}{7.4.1 Explicit Differentiation Methods}
  \begin{blueblock}{\textbf{Individual differentiation}}
    \begin{itemize}
      \item \alert{Individuals} are assigned \alert{different tasks}:
      \begin{itemize}
        \item \footnotesize{some focus on exploitation}
        \item \footnotesize{the others only do the exploration}
      \end{itemize}
    \end{itemize}
  \end{blueblock}
  \begin{greenblock}{Examples}
    \begin{itemize}
      \item locally-informed particle and globally-informed particle in PSO
      \item Individuals with large and small scaling factor in DE
    \end{itemize}
  \end{greenblock}
\end{frame}

\begin{frame}{7.4.1 Explicit Differentiation Methods}
  \begin{blueblock}{\textbf{Evolution stage differentiation}}
    \begin{enumerate}
      \item Split evolution \alert{stage} into \alert{different parts}
      \item Each part have different tendencies on exploitation or exploration
    \end{enumerate}
  \end{blueblock}
  \begin{greenblock}{Examples}
    \begin{enumerate}
      \item Divide the evolution stage into three parts
      \item Exploration is encouraged in the first stage
      \item Exploitation is preferred in the last stage
    \end{enumerate}
  \end{greenblock}
\end{frame}

\subsection{Population Diversity-driven Methods}

\begin{frame}{7.4.2 Population Diversity-driven Methods}
  \begin{blueblock}{\textbf{Threshold value control methods}}
    \begin{itemize}
      \item Use \alert{novelty value}, which indicates an individual's \alert{contribution to the population diversity}, to encourage exploration
    \end{itemize}
  \end{blueblock}
  \begin{greenblock}{Examples}
    \begin{itemize}
      \item Add any individual whose novelty value is larger than the threshold value into the archive solutions
      \item Restrict individuals whose novelty values are smaller than the threshold value from reproduction
    \end{itemize}
  \end{greenblock}
\end{frame}

\begin{frame}{7.4.2 Population Diversity-driven Methods}
  \begin{blueblock}{\textbf{Adaptive parameter control methods}}
    \begin{itemize}
      \item Make the \alert{parameters dependent on the population diversity} based on experimental experience
    \end{itemize}
  \end{blueblock}
  \begin{greenblock}{Examples}
    \begin{itemize}
      \item If the population diversity decreases or the population becomes stagnated:
      \begin{itemize}
        \item \footnotesize{automatically adjust reproduction parameters to enhance exploration}
      \end{itemize}
    \end{itemize}
  \end{greenblock}
  \begin{blueblock}<2->{\textbf{Multi-objective methods}}
    \begin{itemize}
      \item Use \alert{Pareto dominance relationship} in survivor selection to \alert{balance between diversity and fitness} of population
    \end{itemize}
  \end{blueblock}
\end{frame}

\subsection{Non-overlapping Multi-population Methods}
\begin{frame}{7.4.3 Non-overlapping Multi-population Methods}
  \begin{aquote}{Niccol\'{o} Machiavelli}
    a Captain should endeavor with every act to divide the forces of the enemy
  \end{aquote}
  \pause
  \begin{blueblock}{Keypoints}
    \begin{itemize}
      \item \alert{Grouped individuals} into different sub-populations based on their \alert{distance relationship}
      \item \alert{Search areas} of sub-populations are required to be \alert{non-overlapping}
      \item \alert{Each sub-population} is expected to \alert{search for a distinct optimum}
    \end{itemize}
  \end{blueblock}
  \begin{yellowblock}{Tips}
    \begin{itemize}
      \item Similar to the \alert{mixture} of \alert{inbreeding mating} and \alert{restricted competition}
    \end{itemize}
  \end{yellowblock}
\end{frame}

\subsection{Space Partitioning-based Methods}

\begin{frame}{7.4.4 Space Partitioning-based Methods}
  \begin{aquote}{Niccol\'{o} Machiavelli}
    a Captain should endeavor with every act to divide the forces of the enemy
  \end{aquote}
  \begin{blueblock}{Keypoints}
    \begin{itemize}
      \item \alert{Partition} the search space
      \item \alert{Frequently visited areas} can be \alert{identified by sub-spaces}
      \item Following \alert{searches in these areas} will be \alert{prohibited or inhibited}
    \end{itemize}
  \end{blueblock}
  \begin{yellowblock}{Tips}
    \begin{itemize}
      \item Similar to non-visiting or \alert{tabu search}
    \end{itemize}
  \end{yellowblock}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \tableofcontents[currentsection,hideallsubsections]
\end{frame}

\begin{frame}{7.5 Discussions}
  \begin{blueblock}{\textbf{Measurements of exploration and exploitation}}
    \begin{itemize}
      \item \alert{Measures of exploration and exploitation} are prerequisite to control the balance between them
      \item \alert{Relationship} between the \alert{population diversity} and the \alert{balance between exploration and exploitation} is not clear
    \end{itemize}
  \end{blueblock}
  \begin{blueblock}<2->{\textbf{Control of exploration and exploitation}}
    \begin{itemize}
      \item \alert{Many factors comprehensively} contribute to the balance of exploration and exploitation
    \end{itemize}
  \end{blueblock}
  \begin{blueblock}<3->{\textbf{Identification of promising areas}}
    \begin{itemize}
      \item \alert{Learn promising areas} that contain relatively good optima \alert{through the whole search process}
      \item \alert{Build learning models} to predict the degree of promising areas \alert{based on search history}
    \end{itemize}
  \end{blueblock}
\end{frame}

\begin{frame}
\centering
\huge
Thank you!\\
Q\&A
\end{frame}

\end{document}
